#include <iostream>

using namespace std;

bool przedzialy(int &liczba)
{
	if (liczba < 1)
		liczba++;
	else if (liczba > 20 && liczba < 35)
		liczba--;
	else if (liczba >= 35 && liczba <= 50)
		liczba++;
	else if (liczba >= 60)
		liczba--;
	else
		return false;
}

int main()
{
	int liczba;
	cin >> liczba;
	int* l = &liczba;
	while (przedzialy(*l))
	{
		cout << liczba << " ";
	}
	cout << endl;
	system("pause");
	return 0;
}