#include <iostream>

using namespace std;

struct List_part
{
	int data;
	List_part* next;
	List_part* prev;
	List_part();
};

List_part::List_part()
	:data(0),
	next(nullptr),
	prev(nullptr)
{}

/////////////////////////////////////////////////////////////////////////////////
class List
{
	int count;
	List_part* first;
	List_part* last;
public:
	List();
	void push_back(int a);
	void push_front(int a);
	void pop_back();
	void pop_front();
	int size();
	int view(int a);
};

List::List()
	:count(0),
	first(nullptr),
	last(nullptr)
{
}

void List::push_back(int a)
{
	if (first == nullptr)
	{
		first = new List_part;
		last = first;
		last->data = a;
	}
	else
	{
		List_part* temp = new List_part;
		temp->data = a;
		temp->prev = last;
		last->next = temp;
		last = temp;
	}
	count++;
}
void List::push_front(int a)
{
	if (first == nullptr)
	{
		first = new List_part;
		last = first;
		last->data = a;
	}
	else
	{
		List_part* temp = new List_part;
		temp->data = a;
		first->prev = temp;
		temp->next = first;
		first = temp;
	}
	count++;
}
void List::pop_back()
{
	if (count != 0)
	{
		List_part* temp = last->prev;
		delete last;
		last = temp;
	}
	count--;
}
void List::pop_front()
{
	if (count != 0)
	{
		List_part* temp = first->next;
		delete first;
		first = temp;
	}
	count--;
}
int List::size()
{
	return count;
}
int List::view(int a)
{
	if (a < count)
	{
		List_part* temp = first;
		for (int i = 0;i < a;i++)
		{
			temp = temp->next;
		}
		return temp->data;
	}
	else
		return -1;
}
////////////////////////////////////////////////////////////////////////////////
int main()
{
	List lista;
	lista.push_back(666);
	lista.push_back(665);
	lista.push_front(616);
	lista.pop_front();
	for (int i = 0;i < lista.size();i++)
		cout << lista.view(i)<< endl;
	return 0;
}
