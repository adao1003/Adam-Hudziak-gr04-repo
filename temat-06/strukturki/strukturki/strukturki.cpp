// strukturki.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;

typedef int TAB11[11];
struct Osoba
{
	string nazwisko;
	int rok;
	bool plec;
	TAB11 pesel;
};
void wypisz(Osoba& os)
{
	cout << os.nazwisko;
	cout << endl;
	cout << os.rok;
	cout << endl;
	cout << os.plec;
	cout << endl;
	for (int i = 0;i < 11;i++)
		cout << os.pesel[i];
	cout << endl;
}
int main()
{
	Osoba os;
	wypisz(os);
    return 0;
}	