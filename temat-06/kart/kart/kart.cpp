// kart.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

void func(int &a, int *b, int c)
{
	++a;
	++*b;
	++c;
}

int main()
{
	int* a = new int;
	int* b = new int;
	int* c = a;
	*a = 7;
	*b = 12;
	*c = 0;
	func(*a, b, *c);
	cout << *a << " " << *b << " " << *c << " ";
	cin.get();
    return 0;
}

