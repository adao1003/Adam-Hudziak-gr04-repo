#ifndef LOGICCIRCUIT_H_INCLUDED
#define LOGICCIRCUIT_H_INCLUDED
#include <string>
using namespace std;

struct LogicGate
{
    int gate_nr;
    LogicGate* IN1;
    LogicGate* IN2;
    string GateType;
    bool solved;
    bool OUT;
    LogicGate();
    bool solve();
};
struct List_part
{
    LogicGate* gate;
    List_part* next;
    List_part();
};
struct List
{
    int count;
    List_part* head;
    List_part* tail;
    List();
    void push_back(LogicGate* gate);
    LogicGate* find_gate(int nr_gate);
    void view();
    int size();
    LogicGate* get(int nr);
};
struct LogicCircuit
{
    List* GateList; //lista bramek
    List* Input; //lista wejsc
    List* Output; //lista wyjsc
    LogicCircuit();
    bool solver(LogicGate* gate);
    void addInput(int Node_number);
    void addGate(string type, int OUT_node, int IN1_node, int IN2_node);
    void setOutput(int Node_number);
    void set_input_state(int Node_number, bool state);
    bool solveCicuit();
    void resetCircuit();
    void deleteCircuit();
    ~LogicCircuit();
};

#endif // LOGICCIRCUIT_H_INCLUDED
