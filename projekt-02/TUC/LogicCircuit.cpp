#include "LogicCircuit.h"
#include <iostream>
#include <string>
LogicGate::LogicGate():gate_nr(0), IN1(nullptr), IN2(nullptr), GateType(""), solved(0), OUT(0) {}
bool LogicGate::solve()
{
    if(solved==1)
        return true;
    if(GateType=="NOT"&&IN1->solved)
    {
        OUT=!IN1->OUT;
        solved=true;
        return true;
    }
    else
    {
        if(IN1->solved||IN2->solved)
        {
            if     (GateType=="AND")  OUT=IN1->OUT&&IN2->OUT;
            else if(GateType=="NAND") OUT=!(IN1->OUT&&IN2->OUT);
            else if(GateType=="OR")   OUT=IN1->OUT||IN2->OUT;
            else if(GateType=="NOR")  OUT=!(IN1->OUT||IN2->OUT);
            else if(GateType=="XOR")  OUT=(IN1->OUT&&!IN2->OUT)||(!IN1->OUT&&IN2->OUT);
            else if(GateType=="XNOR") OUT=!((IN1->OUT&&!IN2->OUT)||(!IN1->OUT&&IN2->OUT));
            solved=true;
            return true;
        }
    }
    return false;
}
List_part::List_part(): gate(nullptr), next(nullptr)  {}

List::List(): count(0), head(nullptr), tail(nullptr) {}
void List::push_back(LogicGate* gate)
{
    if(head==nullptr)
    {
        head=new List_part;
        tail=head;
        head->gate=gate;
    }
    else
    {
        tail->next=new List_part;
        tail->next->gate=gate;
        tail=tail->next;
    }
    count++;
}
LogicGate* List::find_gate(int nr_gate)
{
    List_part* temp=head;
    while(temp!=nullptr)
    {
        if(temp->gate->gate_nr==nr_gate)
            return temp->gate;
        temp=temp->next;
    }
    return nullptr;
}
void List::view()
{
    List_part* temp =head;
    while(temp!=nullptr)
    {
        cout << temp->gate->gate_nr << " " << temp->gate->GateType << " " << temp->gate->OUT << " " << temp->gate->solved<<endl;
        temp=temp->next;
    }
}
int List::size()
{
    return count;
}
LogicGate* List::get(int nr)
{
    if(nr>=count)
        return nullptr;
    List_part* temp= head;
    for(int i=0;i<nr;i++)
        temp=temp->next;
    return temp->gate;
}
LogicCircuit::LogicCircuit()
 {
     GateList=new List;
     Input=new List;
     Output=new List;
 }
void LogicCircuit::addInput(int Node_number)
{
    LogicGate* temp=new LogicGate;
    temp->gate_nr=Node_number;
    temp->GateType="IN";
    GateList->push_back(temp);
    Input->push_back(temp);
}
void LogicCircuit::addGate(string type, int OUT_node, int IN1_node, int IN2_node)
{
    LogicGate* temp=new LogicGate;
    temp->GateType=type;
    temp->gate_nr=OUT_node;
    temp->IN1=GateList->find_gate(IN1_node);
    if(type!="NOT")
        temp->IN2=GateList->find_gate(IN2_node);
    GateList->push_back(temp);
}
void LogicCircuit::setOutput(int Node_number)
{
    Output->push_back(GateList->find_gate(Node_number));
}
void LogicCircuit::set_input_state(int Node_number, bool state)
{
    LogicGate* temp=Input->find_gate(Node_number);
    if(temp!=nullptr)
    {
        temp->OUT=state;
        temp->solved=true;
    }

}
bool LogicCircuit::solver(LogicGate* gate)
{
    if(gate==nullptr)
        return false;
    if(gate->solved)
        return true;
    solver(gate->IN1);
    solver(gate->IN2);
    if(gate->solve())
        return true;
    return false;
}
bool LogicCircuit::solveCicuit()
{
    int max=Output->size();
    for(int i=0;i<max;i++)
    {
        if(!solver(Output->get(i)))
            return false;
    }
    return true;
}
void LogicCircuit::resetCircuit()
{
    List_part* temp=GateList->head;
    while(temp!=nullptr)
    {
        temp->gate->solved=false;
        temp=temp->next;
    }
}
void LogicCircuit::deleteCircuit()
{
    if(GateList!=nullptr)
        return;
    List_part* temp=GateList->head;
    while(GateList->head!=nullptr)
    {
        temp=temp->next;
        delete GateList->head->gate;
        delete GateList->head;
        GateList->head=temp;
    }
    temp=Input->head;
    while(Input->head!=nullptr)
    {
        temp=temp->next;
        delete Input->head;
        Input->head=temp;
    }
    temp=Output->head;
    while(Output->head!=nullptr)
    {
        temp=temp->next;
        delete Output->head;
        Output->head=temp;
    }
    delete Input;
    delete Output;
    delete GateList;
    GateList==nullptr;
}
LogicCircuit::~LogicCircuit()
{
    deleteCircuit();
}
