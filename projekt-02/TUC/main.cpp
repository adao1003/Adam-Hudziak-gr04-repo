#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include "LogicCircuit.h"
using namespace std;

bool read_circuit(LogicCircuit& lg, char* circuit)
{
    ifstream file(circuit);
    stringstream ss;
    string buffer;
    vector<int> out;
    if(!file.is_open())
        return false;
    while (!file.eof())
    {
        getline(file,buffer);
        ss.str(buffer);
        ss >> buffer;
        if(buffer=="IN:")
        {
            int temp;
            while (!ss.eof())
            {
                ss >> temp;
                lg.addInput(temp);
            }
        }
        else if(buffer=="OUT:")
        {
            int temp;
            ss >> temp;
            out.push_back(temp);
        }
        else
        {
            int IN1, IN2, OUT;
            ss >> IN1 >> IN2 >> OUT;
            lg.addGate(buffer, OUT,IN1,IN2);
        }
        ss.clear();
    }
    for(int i=0;i<out.size();i++)
    {
        lg.setOutput(out[i]);
    }
    return true;
}
void write_to_file(LogicCircuit& lg, char* outfile)
{
    fstream file(outfile, ios::app|ios::out);
    lg.solveCicuit();
    file << "IN: ";
    LogicGate* temp = nullptr;
    for(int i=0;i<lg.Input->count;i++)
    {
        temp=lg.Input->get(i);
        file <<temp->gate_nr << ":" << temp->OUT << " ";
    }
    file << "OUT: ";
    for(int i=0;i<lg.Output->count;i++)
    {
        temp=lg.Output->get(i);
        file <<temp->gate_nr << ":" << temp->OUT << " ";
    }
    file << endl;
    lg.resetCircuit();
}
bool read_state(LogicCircuit& lg, char* state, char* outfile)
{
    ifstream file(state);
    stringstream ss, ss2;
    string buffer;
    if(!file.is_open())
        return 0;
    while (!file.eof())
    {
        getline(file,buffer);
        ss.str(buffer);
        while(!ss.eof())
        {
            ss >> buffer;
            int gate;
            bool state;
            for(int i=0;i<buffer.size();i++)
            {
                if(buffer[i]==':')
                {
                    ss2.clear();
                    ss2.str("");
                    ss2 << buffer.substr(0,i);
                    ss2 >> gate;
                    ss2.clear();
                    ss2.str("");
                    ss2 << buffer.substr(i+1,1);
                    ss2 >> state;
                    break;
                }
            }
            //cout << gate << endl << state;
            lg.set_input_state(gate,state);
        }
        write_to_file(lg, outfile);
        ss.clear();
    }
}
int main(int argc, char* argv[])
{
    LogicCircuit lg;
    if(argc!=4)
    {
        cout << argv[0] <<  " Plik_ukladu Plik_stanow Plik_wyjsciowy";
    }
    if(!(read_circuit(lg,argv[1])))
    {
        cout << "B�edny plik";
        return 666;
    }
    if(!(read_state(lg,argv[2], argv[3])))
    {
        cout << "B�edny plik";
        return 666;
    }
    lg.deleteCircuit();
    return 0;
}
