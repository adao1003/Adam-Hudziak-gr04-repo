#include <iostream>
#include <ctime>

using namespace std;


struct tree_part
{
	int data;
	int counter;
	tree_part* right;
	tree_part* left;
};
void add(tree_part*& root, int liczba)
{
	if (root == nullptr) {
		root = new tree_part;
		root->data = liczba;
		root->counter = 1;
		root->left = nullptr;
		root->right = nullptr;
	}
	else
	{
		if (root->data > liczba)
			add(root->left, liczba);
		else if (root->data == liczba) {
			root->counter = root->counter++;
			return;
		}
		else
			add(root->right, liczba);
	}
}
void write(tree_part* root)
{
	if (root == nullptr)
		return;
	write(root->left);
	cout << root->data << "(" << root->counter << ")" << endl;
	write(root->right);
}
void delete_tree(tree_part*& root)
{
	if (root == nullptr) return;
	delete_tree(root->left);
	delete_tree(root->right);
	delete root;
}
int main()
{
	srand(time(NULL));
	tree_part* root = NULL;
	for (int i = 0;i < 10000;i++)
		add(root, rand()%100);
	//write(root);
	delete_tree(root);
//	write(root);
	cin.get();
}