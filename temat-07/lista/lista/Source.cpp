#include <iostream>
#include <string>
using namespace std;

struct Osoba {
	char* nazwisko;
	int rok_ur;
	bool plec;
	Osoba* next;
	Osoba() :nazwisko(nullptr), rok_ur(0), plec(0) {}
	Osoba(char* n, int u, bool p) : rok_ur(u), plec(p), next(nullptr) 
	{
		nazwisko = new char[strlen(n) + 1];
		strcpy_s(nazwisko, strlen(n) + 1, n);
	}
};
class list
{
	int count;
	Osoba* head;
	Osoba* tail;
public:
	list() : count(0), head(nullptr) {}
	void push_back(Osoba &os);
	void push_front(Osoba &os);
	void push_sort(Osoba &os);
	void delete_name(char* name);
	void delete_list();
	void view();
	int count_female();
	int size();
};
void list::push_back(Osoba &os)
{
	if (head == nullptr)
	{
		head = new Osoba;
		*head = os;
		head->next = nullptr;
		tail = head;
	}
	else
	{
		Osoba* temp=new Osoba;
		*temp = os;
		temp->next = nullptr;
		tail->next = temp;
		tail = temp;
	}
	count++;
}
void list::push_front(Osoba &os)
{
	Osoba* temp = new Osoba;
	*temp = os;
	temp->next = head;
	head = temp;
}
void list::push_sort(Osoba &os)
{
	if (head == nullptr)
	{
		head = new Osoba;
		*head = os;
		tail = head;
	}
	else if (strcmp(head->nazwisko, os.nazwisko) > 0)
	{
		Osoba* temp = new Osoba;
		*temp = os;
		temp->next = head;
		head = temp;
	}
	else
	{
		Osoba* temp = head;
		while (temp->next != nullptr && strcmp(temp->next->nazwisko,os.nazwisko)<0)
		{
			temp = temp->next;
		}
	}

}

void list::delete_name(char* name)
{
	Osoba* temp = head;
	if (strcmp(name, temp->nazwisko) == 0)
	{
		head = head->next;
		delete temp;
		return;
	}
	while (temp != nullptr) {
		if (strcmp(name, temp->next->nazwisko) == 0)
		{
			Osoba* temp2 = temp->next->next;
			delete temp->next;
			temp->next = temp2;
			return;
		}
		temp = temp->next;
	}
}
void list::delete_list()
{
	Osoba* temp = head->next;
	while (temp != NULL)
	{
		delete head;
		head = temp;
		temp = temp->next;
	}
	delete head;
	head = nullptr;
}

void list::view()
{
	Osoba* temp = head;
	while (temp != nullptr)
	{
		cout << temp->nazwisko << " " << temp->plec << " " << temp->rok_ur << endl;
		temp = temp->next;
	}

}
int list::count_female()
{
	int i = 0;
	Osoba* temp = head;
	while (temp != nullptr) {
		if (temp->plec == 0)
			i++;
		temp = temp->next;
	}
	return i;
}

int list::size()
{
	return count;
}
int main() {
	list lista;
	Osoba os("Zbigniew", 1969, 1);
	Osoba o("AA", 666, 0);
	Osoba o2("Ala", 616, 0);
	lista.push_back(os);
	lista.push_back(o);
	lista.push_back(o2);
	//lista.delete_name("Zbigniew");
	lista.delete_list();
	lista.view();
	cin.get();
	return 0;
}