#include <iostream>

using namespace std;

void zwieksz(int& a, int& b)
{
	a++;
	b++;
}

int main()
{
	int a = 12;
	int b = 14;
	zwieksz(a, b);
	cout << a << " " << b;
}