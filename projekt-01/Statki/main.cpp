#include <iostream>
#include <cstdlib>
#include <fstream>
#include <chrono>
#include <random>
#include <vector>
#define rand_t 10
#define SIZETAB 100
using namespace std;

struct Poz2D
{
    unsigned int x;
    unsigned int y;
};

void help();
bool ship_rand(int, int, int[][SIZETAB], Poz2D,default_random_engine);
void write_to_file (int[][SIZETAB], ofstream&, Poz2D);
void error (int=0);

int main(int argc, char* argv[])
{
    int tab[SIZETAB][SIZETAB]={};
    int ship{}, ship_count{}; // ship - wymiary statku, ship_count - liczba statkow
    ifstream in;
    ofstream out;
    Poz2D dim; //wymiary planszy
    auto seed = chrono::system_clock::now().time_since_epoch().count();
    default_random_engine generator(seed); //generator pseudolosowy

    if (argc !=3||argv[1]=="-h"){ //Sprawdzanie argument�w
        help();
        return 0;
    }
    else // otwarcie plikow
    {
        in.open(argv[1]);
        out.open(argv[2]);
    }
    if (in&&out)
    {
        //glowny program
        in >> dim.x >> dim.y;
        if(in.bad())
            return 0;
        while(!in.eof())
        {
            if(!in.good())
            error();
            in >> ship >> ship_count;
            if(in.good()){
                if(!ship_rand(ship, ship_count, tab, dim, generator))   return 666;}
            else if(!in.eof())
                return 0;
        }
        write_to_file(tab, out, dim);
    }
    else
       error(1);

//    system("pause");
    in.close();
    out.close();
    return 0;
}

void error(int e) //funkcja bledow
{
    if (e==0)
        cout << "bledny typ danych" << endl;
    if (e==1)
         cout << "Blad odczytu/zapisu pliku" << endl;
    if (e==2)
         cout << "bledna liczba statkow" << endl;
}
void help()
{
    cout << "Statki.exe plik_wejsciowy plik_wyjsciowy" << endl;
    //wypisuje -h
}
bool push_ship(int tab[][SIZETAB], Poz2D poz, int i,bool ver) //stawia statek
{
    if(i==0)
        return 1;
    Poz2D temp = poz;
    if (ver)
        temp.x--;
    else
        temp.y--;
    if(tab[poz.x][poz.y]==0)
        if(push_ship(tab,temp,i-1,ver))
        {
            tab[poz.x][poz.y]=1;
            return 1;
        }
    return 0;
}

void make_banned_area(int tab[][SIZETAB], Poz2D poz,int i,bool ver) //tworzy tzw. obszar zabroniony (zabezpieczenie przed strykaniem sie statk�w)
{
    if (i==0)
        return;
    if(tab[poz.x-1][poz.y-1]==0&&poz.x!=0&&poz.y!=0) tab[poz.x-1][poz.y-1]=2;
    if(tab[poz.x-1][poz.y]==0&&poz.x!=0)             tab[poz.x-1][poz.y]=2;
    if(tab[poz.x-1][poz.y+1]==0&&poz.x!=0)           tab[poz.x-1][poz.y+1]=2;
    if(tab[poz.x][poz.y-1]==0&&poz.y!=0)             tab[poz.x][poz.y-1]=2;
    if(tab[poz.x][poz.y]==0)                         tab[poz.x][poz.y]=2;
    if(tab[poz.x][poz.y+1]==0)                       tab[poz.x][poz.y+1]=2;
    if(tab[poz.x+1][poz.y-1]==0&&poz.y!=0)           tab[poz.x+1][poz.y-1]=2;
    if(tab[poz.x+1][poz.y]==0)                       tab[poz.x+1][poz.y]=2;
    if(tab[poz.x+1][poz.y+1]==0)                     tab[poz.x+1][poz.y+1]=2;
    if(ver) make_banned_area(tab,{poz.x-1,poz.y},i-1,ver);
    else make_banned_area(tab,{poz.x,poz.y-1},i-1,ver);
}

bool ship_rand(int ship, int ship_count, int tab[][SIZETAB], Poz2D dim, default_random_engine gen) //losowanie pozycji statkow
{
    uniform_int_distribution<int> horizontal(0,ship_count);
    vector<Poz2D> poz; //przechwuje wspolrzedne mozliwych pozycji okretow
    int c=0;
    int hor = horizontal(gen); //liczba statk�w poziomo
    int ver = ship_count-hor; //liczba statk�w pionowo
    for(int i=0;i<dim.x;i++, c=0)
        for(int j=0;j<dim.y;j++){
            if (tab[i][j]==0) c++;
            else c=0;
            if (c>ship) poz.push_back({i,j});
        }
    if (poz.size()==0){
        error(2);
        return false;
    }
    uniform_int_distribution<int> random1(0,poz.size()-1);
    int r; //bufor liczby losowej
    int t = rand_t; //liczba prob losowania
    while(hor>0)
    {
        do {
           r = random1(gen);
           t--;
           if (t==0){
                error(2);
                return false;}
        } while(!push_ship(tab, poz[r], ship, 0));
        make_banned_area(tab,poz[r],ship, 0);
        hor--;
        t=rand_t;
    }

    c=0;
    poz.clear();

    for(int i=0;i<dim.y;i++, c=0)
        for(int j=0;j<dim.x;j++){
            if (tab[j][i]==0) c++;
            else c=0;
            if (c>ship) poz.push_back({j,i});
        }
    if (poz.size()==0)
        error(2);
    uniform_int_distribution<int> random2(0,poz.size()-1);
    t=rand_t;
    while(ver>0)
    {
        do {
           r = random2(gen);
           t--;
            if (t==0){
                error(2);
                return false;}
        } while(!push_ship(tab, poz[r], ship, 1));
        make_banned_area(tab,poz[r],ship, 1);
        ver--;
        t=rand_t;
    }
    return true;
}
void write_to_file (int tab[][SIZETAB], ofstream& out, Poz2D dim)
{
    for(int i=0;i<dim.x;i++){
        for(int j=0;j<dim.y;j++){
            if(tab[i][j]==1) out << "X";
            else out << " ";
        }
        out << endl;
    }
}
