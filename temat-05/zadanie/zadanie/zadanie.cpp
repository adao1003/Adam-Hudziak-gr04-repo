#include "stdafx.h"
#include <iostream>
#include <ctime>
#define SIZETAB 30

using namespace std;

void init(int tab[], int size = SIZETAB)
{
	for (int i = 0;i < size;i++)
		tab[i] = rand();
}
void draw(int tab[], int size = SIZETAB)
{
	for (int i = 0;i < size;i++)
		cout << tab[i] << " ";
}
void swap(int tab[], int j)
{
	int temp = tab[j - 1];
	tab[j - 1] = tab[j];
	tab[j] = temp;
}

void sort(int tab[])
{
	for (int i = 0;i < SIZETAB;i++)
		for (int j = SIZETAB - 1;j > i;j--)
			if (tab[j - 1] > tab[j])
				swap(tab, j);
}
int main()
{
	srand(time(NULL));
	int tab[SIZETAB];
	init(tab);
	sort(tab);
	draw(tab);
	return 0;
}

